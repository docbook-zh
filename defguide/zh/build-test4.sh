#!/bin/sh

cd /home/dongsheng/var/svn/docbook/defguide/zh

/bin/rm -fr build; ant defguide
/bin/mv  build/defguide.pdf defguide-fop.pdf

/bin/rm -fr build; ant defguide-xep
/bin/mv  build/defguide.pdf defguide-xep.pdf


/bin/rm -fr build; ant translate; /bin/mv build/defguide-zh.xml defguide-zh.xml


/bin/rm -fr build; /bin/mkdir build; /bin/cp defguide-zh.xml build/defguide-zh.xml
ant pdf; /bin/mv build/defguide-zh.pdf defguide-zh-fop.pdf

/bin/rm -fr build; /bin/mkdir build; /bin/cp defguide-zh.xml build/defguide-zh.xml
ant pdf-xep; /bin/mv build/defguide-zh.pdf defguide-zh-xep.pdf

/bin/rm -fr build; /bin/mkdir build; /bin/cp defguide-zh.xml build/defguide-zh.xml
ant html

ant defguide-html
