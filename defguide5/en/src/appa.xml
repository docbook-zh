<?xml version="1.0" encoding="utf-8"?>
<appendix xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="app-install">
<?dbhtml filename="appa.html"?>
<info>
  <title>Installation</title>
  <pubdate>$Date: 2007-07-22 02:32:03 +0800 (Sun, 22 Jul 2007) $</pubdate>
  <releaseinfo>$Revision: 7106 $</releaseinfo>
</info>

<section xml:id="s-install">
<title>Installing DocBook</title>

<section xml:id="s-installrng">
<title>Installing the DocBook RELAX NG Grammar</title>

<para>FIXME: tbd.</para>

</section>

<section xml:id="s-installdtd">
<title>Installing the DocBook DTD</title>

<para>FIXME: tbd.</para>

</section>

<section xml:id="s-installxsd">
<title>Installing the DocBook W3C XML Schema</title>

<para>FIXME: tbd.</para>

</section>

<section xml:id="s-installsch">
<title>Installing the DocBook Schematron Schema</title>

<para>FIXME: tbd.</para>

</section>

<section xml:id="s.getting-iso-entities">
<title>Getting the ISO Entity Sets</title>

<para>
<indexterm significance="normal"><primary>entity sets</primary>
  <secondary>ISO standard, obtaining</secondary></indexterm>
<indexterm significance="normal"><primary>entities</primary>
  <secondary>entity sets</secondary><see>entity sets</see></indexterm>
<indexterm significance="normal"><primary>ISO standards</primary>
  <secondary>entity sets</secondary>
    <tertiary>obtaining</tertiary></indexterm>
<indexterm significance="normal"><primary>OASIS</primary>
  <secondary>entity sets (ISO standard), obtaining</secondary></indexterm>

DocBook refers to a number of standard entity sets that
are not distributed with DocBook. (They aren't distributed with
DocBook because they aren't maintained by the DocBook
TC. They're maintained by ISO.) If you've installed other SGML
DTDs or tools, they may already be on your system.</para>
<para>If you are missing some of them, they are available from
Robin Cover's pages at OASIS: <link xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="http://www.oasis-open.org/cover/ISOEnts.zip">
http://www.oasis-open.org/cover/ISOEnts.zip</link>.<footnote>
<para>The names of the entity files in this distribution do not
exactly match the names of the files used in the catalog file
distributed with DocBook (<filename>docbook.cat</filename>). Make sure your
catalog file points to the right files.</para></footnote> See <link xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="http://www.oasis-open.org/cover/topics.html#entities">http://www.oasis-open.org/cover/topics.html#entities</link>
for more information.
</para>

</section>
<section xml:id="dbcatalog"><info><title>The DocBook Catalog</title></info>

<para>
<indexterm significance="normal"><primary>public identifiers</primary>
  <secondary>DocBook DTD</secondary>
    <tertiary>V3.1</tertiary></indexterm>
<indexterm significance="normal"><primary>DocBook DTD</primary>
  <secondary>public identifiers</secondary></indexterm>

DocBook uses public identifiers to refer to its
constituent parts. In some sense, DocBook <emphasis>is</emphasis>
DocBook because it has the formal public
identifier <quote><literal>-//OASIS//DTD DocBook V3.1//EN</literal></quote>.
In order for tools on your system to
find your locally installed copy of DocBook, you must map these
public identifiers into system identifiers, i.e., filenames, on
your system. For a complete discussion of catalog files, 
see <xref linkend="s-pid-sid-catalogs"/>.</para>

<para>
<indexterm significance="normal"><primary>catalog files</primary>
  <secondary>DocBook distribution, installing</secondary></indexterm>

The DocBook distribution includes a sample catalog, <filename>docbook.cat
</filename>, which provides a mapping for all of the public identifiers referenced
by DocBook. This mapping won't work <quote>out of the box</quote> for two
reasons: first, your tools won't be able to find it, and second, the mappings
for the ISO entity sets probably don't point to the right place on your system.
</para>

<section xml:id="s.findingcatalog">
<title>Finding the Catalog</title>

<para>If you've already got some other SGML DTDs installed, you probably already
have a catalog file. In this case, the easiest thing to do is append
the DocBook catalog entries to the end of your existing catalog and then change
them to point to the files on your system.</para>

<para>If DocBook is the first DTD that you're installing, make a
copy of <filename>docbook.cat</filename> and call it
<filename>catalog</filename>. Put this file in a higher-level
directory and edit the relative pathnames that it contains to point to
the actual locations of the files on your system. For example, if you installed DocBook
in <filename>/share/sgml/docbk30/</filename>, put the
<filename>catalog</filename> in <filename>/share/sgml/</filename>.</para>

<para>
<indexterm significance="normal"><primary>environment</primary>
  <secondary>variables, setting (catalog files)</secondary></indexterm>
<indexterm significance="normal"><primary>SGML_CATALOG_FILES environment variable, setting</primary></indexterm>

In order for applications to find your catalog file(s), you may have
to change the application preferences or set an environment variable. For <application>
SP</application> and <application>Jade</application>, set the environment
variable <envar>SGML_CATALOG_FILES</envar> to the delimited list of catalog
filenames. On my system, this looks like:</para>
<screen>SGML_CATALOG_FILES=./catalog;n:/share/sgml/catalog;n:/adept80/doctypes/catalog.jade;j:/jade/catalog
</screen>
<para>
(On a UNIX machine, use colons instead of semicolons to delimit the
filenames.)</para>
<para>If you don't wish to set the environment variable, you can explicitly
pass the name of each catalog to the <application>SP</application> application
with the <option>-c</option> option, like this:</para>
<screen>nsgmls -c ./catalog -c n:/share/sgml/catalog <replaceable>-c othercatalogs
</replaceable> ...</screen>
</section>
<section xml:id="s.fixingcatalog">
<title>Fixing the Catalog</title>

<para>
<indexterm significance="normal"><primary>catalog files</primary>
  <secondary>mapping to system</secondary></indexterm>

The basic format of each entry in the DocBook catalog is:</para>
<screen>PUBLIC "some public id" "some filename"</screen>
<para>What you have to do is change each of the <quote>some filenames</quote>
to point to the actual name of the file on your system.</para>
<note><info/>
<para>
<indexterm significance="normal"><primary>filenames</primary>
  <secondary>catalog file</secondary></indexterm>

Filenames should be supplied using absolute filenames,
or paths relative to the location of
the <emphasis>catalog</emphasis> file.</para>
</note>
<para>To continue with the example above, let's say that you've got:</para>
<itemizedlist>
<listitem><para>DocBook in <filename>/share/sgml/docbk30/</filename>,</para>
</listitem>
<listitem><para>The ISO entities in <filename>/share/sgml/entities/8879/</filename>,
and</para>
</listitem>
<listitem><para>Your catalog in <filename>/share/sgml/catalog</filename></para>
</listitem>
</itemizedlist>
<para>Then you would change the catalog entry for the DTD to be:</para>
<screen>PUBLIC "-//OASIS//DTD DocBook V3.1//EN" "docbk30/docbook.dtd"
</screen>
<para>You would change the catalog entry for the general technical character entities to:
</para>
<screen>PUBLIC "ISO 8879:1986//ENTITIES General Technical//EN" "entities/8879/iso-tech.gml"
</screen>
<para>And similarly for the other public identifiers used by DocBook. In each
case, the filename specified for the public identifier should be the name
of the file on your system, specified as an absolute filename, or 
relative to the location of the <filename>catalog</filename>
in which it occurs.</para>
</section>

<section xml:id="s.mappingsystemids">
<title>Mapping System Identifiers for XML</title>

<para>
<indexterm significance="normal"><primary>XML</primary>
  <secondary>system identifiers</secondary>
    <tertiary>mapping</tertiary></indexterm>
<indexterm significance="normal"><primary>system identifiers</primary>
  <secondary>XML</secondary>
    <tertiary>mapping for</tertiary></indexterm>
Since XML documents are required to have system identifiers, but are not
required to have public identifiers, it's likely that some of the documents
you want to process will only have system identifiers.
</para>
<para>
It turns out that you can still take advantage of the catalog in this case.
The <literal>SYSTEM</literal> directive allows you to map the system
identifier used in the document to the actual location on your system.
</para>
<para>
Suppose that you work with a colleague who uses the system identifier
<quote>file:///c:/sgml/db3xml/db3xml.dtd</quote> to identify the XML version
of DocBook on her system.  On your system, you want to map that to
<quote>/share/sgml/db3xml/db3xml.dtd</quote>.  The following entry in
your catalog will do the trick:
</para>
<screen>
SYSTEM "http://docbook.org/docbook/xml/1.4/db3xml.dtd" "/share/sgml/db3xml/db3xml.dtd"
</screen>
<para>
Unfortunately, this technique only works with applications that read and
understand catalog files.
</para>
</section>
</section>

<section xml:id="s.testinginstall">
<title>Testing Your Installation</title>

<para>FIXME: tbd.</para>
</section>
</section>

</appendix>