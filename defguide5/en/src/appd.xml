<?xml version="1.0" encoding="utf-8"?>
<appendix xmlns="http://docbook.org/ns/docbook"
	  xmlns:xlink="http://www.w3.org/1999/xlink"
	  version="5.0" xml:id="app-resources">
<?dbhtml filename="appd.html"?>
<info>
  <title>Resources</title>
  <pubdate>$Date: 2009-01-05 18:11:06 +0800 (Mon, 05 Jan 2009) $</pubdate>
  <releaseinfo>$Revision: 8202 $</releaseinfo>
</info>

<indexterm significance="normal"><primary>XML</primary>
  <secondary>resources, reference</secondary></indexterm>
<indexterm significance="normal"><primary>SGML</primary>
  <secondary>resources, reference</secondary></indexterm>

<para>The quantity of information about <acronym>SGML</acronym> and <acronym>XML</acronym> is growing
on a daily basis.  This appendix strives to provide both a
complete bibliography of the references mentioned explicitly in
this book, and a sampling of resources for additional
information about DocBook and about <acronym>SGML</acronym> and <acronym>XML</acronym> in general.
Although not all of these resources are focused specifically on
DocBook, they still provide helpful information for DocBook
users.</para>

<section xml:id="latestver"><info><title>Latest Versions of DocBook</title></info>

<para>As of July 1998, responsibility for the advancement and maintenance
of the DocBook <acronym>DTD</acronym> has been transferred from the Davenport Group, which originated
it, to the DocBook Technical Committee of <acronym>OASIS</acronym> (Organization for the Advancement
of Structured Information Standards) at <link xlink:href="http://www.oasis-open.org/">
http://www.oasis-open.org/</link>.</para>

<para>
The latest releases of DocBook can be obtained from the official DocBook
home page at <link xlink:href="http://www.oasis-open.org/docbook/">
http://www.oasis-open.org/docbook/</link>.
</para>
</section>
<section xml:id="res-resources"><info><title>Resources for Resources</title></info>

<para>Here's where to find pointers to the subjects you want to find.</para>
<variablelist>
<varlistentry><term>The Most Recent Version of This Book</term>
<listitem><para>
The most recent online version of this book can be found at
<link xlink:href="http://docbook.org/">http://docbook.org/</link>.
</para></listitem>
</varlistentry>
<varlistentry><term>The Most Recent Version of DocBook</term>
<listitem><para>
The most recent version of DocBook, and the most recent information
about the <acronym>DTD</acronym>, can be found at the DocBook home page:
<link xlink:href="http://www.oasis-open.org/docbook/">http://www.oasis-open.org/docbook/</link>.
</para></listitem>
</varlistentry>
<varlistentry><term>Robin Cover's <acronym>SGML</acronym>/<acronym>XML</acronym> Web page</term>
<listitem>
<para>Easily the largest and most up-to-date list of <acronym>SGML</acronym>/<acronym>XML</acronym> resources; can be found at <link xlink:href="http://www.oasis-open.org/cover/">http://www.oasis-open.org/cover/</link>.</para>
</listitem>
</varlistentry>
<varlistentry><term><systemitem role="newsgroup">comp.text.sgml</systemitem>
and <systemitem role="newsgroup">comp.text.xml</systemitem></term>
<listitem>
<para><acronym>USENET</acronym> newsgroups devoted to <acronym>SGML</acronym> and <acronym>XML</acronym> issues.</para>
</listitem>
</varlistentry>
<varlistentry><term><acronym>FAQ</acronym>s</term>
<listitem>
<para>For pointers to several <acronym>SGML</acronym> <acronym>FAQ</acronym>s, see 
<link xlink:href="http://www.oasis-open.org/cover/general.html#faq">http://www.oasis-open.org/cover/general.html#faq</link>. 
The <acronym>XML</acronym> <acronym>FAQ</acronym> is
available at <link xlink:href="http://www.ucc.ie/xml">http://www.ucc.ie/xml</link>.
</para>
</listitem>
</varlistentry>
<varlistentry><term><link xlink:href="http://www.xml.com/"><acronym>XML</acronym>.com</link></term>
<listitem>
<para><link xlink:href="http://www.xml.com/"><acronym>XML</acronym>.com</link>, run jointly
by Songline Studios and Seybold, is a site devoted to making <acronym>XML</acronym>
accessible.</para>
</listitem>
</varlistentry>
</variablelist>
</section>
<section xml:id="res-intro"><info><title>Introductory Material on the Web</title></info>

<para>These documents provide a good background for a better understanding of
<acronym>SGML</acronym> and <acronym>XML</acronym>.</para>
<variablelist>
<varlistentry><term>A Gentle Introduction to <acronym>SGML</acronym></term>
<listitem>
<para>A useful and simple document available in its original form at <link xlink:href="http://www-tei.uic.edu/orgs/tei/sgml/teip3sg/index.html">http://www-tei.uic.edu/orgs/tei/sgml/teip3sg/index.html
</link>.</para>
</listitem>
</varlistentry>
<varlistentry><term>A Technical Introduction to <acronym>XML</acronym></term>
<listitem>
<para>A close look at the ins-and-outs of <acronym>XML</acronym> is available at <link xlink:href="http://nwalsh.com/docs/articles/xml/">
http://nwalsh.com/docs/articles/xml/</link>.</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section xml:id="res-web"><info><title>References and Technical Notes <?lb?>on the Web</title></info>

<variablelist>
<varlistentry><term>Entity Management</term>
<listitem>
<para><link xlink:href="http://www.oasis-open.org/html/a401.htm"><acronym>OASIS</acronym> Technical
Resolution 9401:1997 (Amendment 2 to <acronym>TR</acronym> 9401)</link>.</para>
<para>
This document describes <acronym>OASIS</acronym> catalog files.
</para>
</listitem>
</varlistentry>
<varlistentry><term>The <acronym>SGML</acronym> Declaration</term>
<listitem>
<para><link xlink:href="http://www.oasis-open.org/cover/wlw11.html">The <acronym>SGML</acronym> Declaration,</link> by Wayne Wholer.
</para>
</listitem>
</varlistentry>
<varlistentry><term>Table Interoperability: Issues for the <acronym>CALS</acronym> Table Model
</term>
<listitem>
<para><link xlink:href="http://www.oasis-open.org/html/a501.htm"><acronym>OASIS</acronym>
Technical Research Paper 9501:1995</link>.</para>
</listitem>
</varlistentry>
<varlistentry><term>Exchange Table Model Document Type Definition</term>
<listitem>
<para><link xlink:href="http://www.oasis-open.org/html/a503.htm"><acronym>OASIS</acronym>
Technical Resolution <acronym>TR</acronym> 9503:1995</link>.</para>
</listitem>
</varlistentry>
<varlistentry xml:id="calsdtd"><term><acronym>CALS</acronym> Table Model Document Type Definition</term>
<listitem>
<para><link xlink:href="http://www.oasis-open.org/html/a502.htm"><acronym>OASIS</acronym>
Technical Memorandum <acronym>TM</acronym> 9502:1995</link></para>
</listitem>
</varlistentry>
<varlistentry xml:id="calsxmldtd">
<term>XML Exchange Table Model Document Type Definition</term>
<listitem>
<para><link xlink:href="http://www.oasis-open-org/html/a901.htm">OASIS
Technical Memorandum TM 9901:1999</link>.</para>
</listitem>
</varlistentry>
<varlistentry xml:id="xhtml">
<term><trademark>XHTML</trademark> 1.0 The Extensible HyperText Markup Language</term>
<listitem>
<para><link xlink:href="http://www.w3.org/TR/xhtml1">W3C Recommendation</link>.
</para>
</listitem>
</varlistentry>
<varlistentry xml:id="html">
<term><trademark>HTML 4.01 Specification</trademark></term>
<listitem>
<para><link xlink:href="http://www.w3.org/TR/html401/">W3C Recommendation</link>.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section xml:id="res-rfc"><info><title>Internet <acronym>RFC</acronym>s</title></info>

<para>
<acronym>RFC</acronym>s (<quote>Request for Comments</quote>) are standards documents
produced by the Internet Engineering Task Force (<acronym>IETF</acronym>).
</para>

<variablelist>
<varlistentry><term><link xlink:href="http://www.cis.ohio-state.edu/htbin/rfc/rfc1630.html"><acronym>RFC</acronym> 1630</link></term>
<listitem>
<para>Universal Resource Identifiers in <acronym>WWW</acronym>.</para>
</listitem>
</varlistentry>

<varlistentry><term><link xlink:href="http://www.cis.ohio-state.edu/htbin/rfc/rfc1736.html"><acronym>RFC</acronym> 1736</link></term>
<listitem>
<para>Functional recommendations for Internet Resource Locators.</para>
</listitem>
</varlistentry>

<varlistentry><term><link xlink:href="http://www.cis.ohio-state.edu/htbin/rfc/rfc1737.html"><acronym>RFC</acronym> 1737</link></term>
<listitem>
<para>Functional requirements for Uniform Resource Names.</para>
</listitem>
</varlistentry>

<varlistentry><term><link xlink:href="http://www.cis.ohio-state.edu/htbin/rfc/rfc1738.html"><acronym>RFC</acronym> 1738</link></term>
<listitem>
<para>Uniform Resource Locators (<acronym>URL</acronym>).</para>
</listitem>
</varlistentry>

<varlistentry><term><link xlink:href="http://www.cis.ohio-state.edu/htbin/rfc/rfc3066.html"><acronym>RFC</acronym> 3066</link></term>
<listitem>
<para>Tags for the identification of languages</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section xml:id="res-spec"><info><title>Specifications</title></info>

<para>Here are pointers to the specifications.</para>
<variablelist>
<varlistentry><term><link xlink:href="http://www.w3.org/TR/REC-xml">The <acronym>XML</acronym> Specification
</link></term>
<listitem>
<para>The <acronym>W3C</acronym> technical recommendation that defines <acronym>XML</acronym> 1.0.</para>
</listitem>
</varlistentry>
<varlistentry><term><link xlink:href="http://www.w3.org/TR/REC-xml-names/">Namespaces
in <acronym>XML</acronym></link></term>
<listitem>
<para>The <acronym>W3C</acronym> technical recommendation that defines <acronym>XML</acronym> namespaces.</para>
</listitem>
</varlistentry>
<varlistentry><term><link xlink:href="http://www.w3.org/TR/REC-MathML/">Mathematical
Markup Language (MathML) 1.0 Specification</link></term>
<listitem>
<para>The <acronym>W3C</acronym> technical recommendation that defines MathML, an <acronym>XML</acronym>
representation of mathematical equations.</para>
</listitem>
</varlistentry>
<varlistentry><term><link xlink:href="http://www.unicode.org/unicode/uni2book/u2.html">
The Unicode Standard, Version 2.0</link></term>
<listitem>
<para>The Unicode standard.</para>
</listitem>
</varlistentry>
<varlistentry><term><link xlink:href="http://www.unicode.org/unicode/reports/tr8.html">
Unicode Technical Report #8</link></term>
<listitem>
<para>Version 2.1 of the Unicode standard.</para>
</listitem>
</varlistentry>

<varlistentry><term><link xlink:href="http://dublincore.org/documents/dces/">
Dublin Core Metadata Element Set, Version 1.1: Reference Description</link></term>
<listitem>
<para>Version 1.1 of the Dublin Core Metadata Initiative's Metadata Element
Set.</para>
</listitem>
</varlistentry>

<varlistentry><term><link xlink:href="http://www.getty.edu/research/tools/vocabulary/tgn/">
Getty Thesaurus of Geographic Names</link></term>
<listitem>
<para>A controlled vocabulary of geographic place names.</para>
</listitem>
</varlistentry>

</variablelist>
</section>
<section xml:id="res-books"><info><title>Books and Printed Resources</title></info>

<para>There are also a number of books worth checking out:</para>
<bibliolist>
<biblioentry xml:id="maler96">
	
	
	<authorgroup>
	  <author><personname><firstname>Eve</firstname><surname>Maler</surname></personname></author>
	  <author><personname><firstname>Jeanne</firstname><surname>El Andaloussi</surname></personname></author>
	</authorgroup>
	<biblioid class="isbn">0-13-309881-8</biblioid>
	<publisher>
	  <publishername>Prentice-Hall PTR</publishername>
	  <address>
	    <city>Upper Saddle River</city>
	    <state>New Jersey</state>
	  </address>
	</publisher>
	<pubdate>1970-01-01<!--1996--></pubdate>
</biblioentry>
<biblioentry>
	
	<authorgroup>
	  <author><personname><firstname>Erik</firstname><surname>van Herwijnen</surname></personname></author>
	</authorgroup>
	<edition>2</edition>
	<biblioid class="isbn">0-7923-9434-8</biblioid>
	<publisher>
	  <publishername>Kluwer Academic Press</publishername>
	</publisher>
	<pubdate>1970-01-01<!--1994--></pubdate>
	<bibliomisc>An introductory book, but not a simple one.</bibliomisc>
</biblioentry>
<biblioentry>
	
	<authorgroup>
	  <author><personname><firstname>Charles</firstname><surname>Goldfarb</surname></personname></author>
	  <author><personname><firstname>Yuri</firstname><surname>Rubinksy</surname></personname></author>
	</authorgroup>
	<biblioid class="isbn">0-7923-9434-8</biblioid>
	<pubdate>1970-01-01<!--1991--></pubdate>
	<publisher>
	  <publishername>Oxford University Press</publishername>
	</publisher>
	<bibliomisc>A reference book by the author of the SGML ISO Standard.</bibliomisc>
</biblioentry>
<biblioentry>
   
   <authorgroup>
     <author><personname><firstname>Martin</firstname><surname>Bryan</surname></personname></author>
   </authorgroup>
   <biblioid class="isbn">0-201-17535-5</biblioid>
   <pubdate>1970-01-01<!--1988--></pubdate>
   <publisher>
      <publishername>Addison-Wesley Publishing Company</publishername>
   </publisher>
 </biblioentry>
<biblioentry>
	
	<authorgroup>
	  <author><personname><firstname>Chet</firstname><surname>Ensign</surname></personname></author>
	</authorgroup>
	<biblioid class="isbn">0-13-226705-5</biblioid>
	<pubdate>1970-01-01<!--1998--></pubdate>
	<publisher>
	  <publishername>Prentice Hall</publishername>
	</publisher>
	<bibliomisc>Effective SGML evangelism.</bibliomisc>
</biblioentry>
<biblioentry>
	
	<authorgroup>
	  <author><personname><firstname>Chris</firstname><surname>Maden</surname></personname></author>
	</authorgroup>
	<biblioid class="isbn">1-56592-518-1</biblioid>
	<pubdate>1970-01-01<!--1999--></pubdate>
	<publisher>
	  <publishername>O'Reilly &amp; Associates</publishername>
	</publisher>
	<bibliomisc>An introductory book about XML.</bibliomisc>
</biblioentry>
<biblioentry>
	
	<authorgroup>
	  <author><personname><firstname>Simon</firstname><surname>St. Laurent</surname></personname></author>
	</authorgroup>
	<biblioid class="isbn">1-5582-8592-X</biblioid>
	<pubdate>1970-01-01<!--1998--></pubdate>
	<publisher>
	  <publishername>MIS:Press/IDG Books Worldwide</publishername>
	</publisher>
	<bibliomisc>Another introductory book about XML.</bibliomisc>
</biblioentry>
<biblioentry>
    
    <authorgroup>
       <author><personname><firstname>Peter</firstname><surname>Flynn</surname></personname></author>
     </authorgroup>
     <biblioid class="isbn">0-7923-8169-6</biblioid>
     <pubdate>1970-01-01<!--1998--></pubdate>
     <publisher>
       <publishername>Kluwer Academic Publishers</publishername>
     </publisher>
<bibliomisc>The standard work on SGML/XML software.</bibliomisc>
</biblioentry>
 <biblioentry>
   
   
   <authorgroup>
      <author><personname><firstname>Michel</firstname><surname>Goosens</surname></personname></author>
      <author><personname><firstname>Sebastian</firstname><surname>Rahtz</surname></personname></author>
   </authorgroup>
   <biblioid class="isbn">0-201-43311-7</biblioid>
   <pubdate>1970-01-01<!--1999--></pubdate>
   <publisher>
     <publishername>Addison-Wesley Publishing Company</publishername>
   </publisher>
 </biblioentry>
</bibliolist>
</section>
<section xml:id="res-tools"><info><title><acronym>SGML</acronym>/<acronym>XML</acronym> Tools</title></info>

<para>An attempt to provide a detailed description of all of the <acronym>SGML</acronym>/<acronym>XML</acronym>
tools available is outside the scope of this book.
</para>
<para>
For a list of recent
of <acronym>SGML</acronym> tools, check out Robin Cover's <acronym>SGML</acronym>/<acronym>XML</acronym> page at <acronym>OASIS</acronym>: <link xlink:href="http://www.oasis-open.org/cover">
http://www.oasis-open.org/cover</link>.
</para>
<para>For a list of <acronym>XML</acronym> tools,
check out <acronym>XML</acronym>.com: <link xlink:href="http://www.xml.com/">http://www.xml.com/</link>.
</para>
</section>
</appendix>
