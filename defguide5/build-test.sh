#!/bin/sh

cd /home/dongsheng/var/svn/docbook/defguide5/en

/bin/rm -fr build; ant pdf
/bin/mv build/defguide5.pdf defguide5-fop.pdf

/bin/rm -fr build; ant pdf-xep
/bin/mv build/defguide5.pdf defguide5-xep.pdf

/bin/rm -fr build; ant html


cd /home/dongsheng/var/svn/docbook/defguide5/zh
/bin/rm -fr build; ant translate; /bin/mv build/defguide5-zh.xml defguide5-zh.xml


/bin/rm -fr build; /bin/mkdir build; /bin/cp defguide5-zh.xml build/defguide5-zh.xml
ant pdf; /bin/mv build/defguide5-zh.pdf defguide5-zh-fop.pdf

/bin/rm -fr build; /bin/mkdir build; /bin/cp defguide5-zh.xml build/defguide5-zh.xml
ant pdf-xep; /bin/mv build/defguide5-zh.pdf defguide5-zh-xep.pdf

/bin/rm -fr build; /bin/mkdir build; /bin/cp defguide5-zh.xml build/defguide5-zh.xml
ant html
